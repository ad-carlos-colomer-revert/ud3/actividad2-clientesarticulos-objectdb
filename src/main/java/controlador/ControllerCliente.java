package controlador;

import controladorBD.ConexionBD;

import dao.clientesDAO;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import modelo.Cliente;

public class ControllerCliente implements Initializable {

    private final static int MODO_NAVEGACION = 0;
    private final static int MODO_NUEVO_REGISTRO = 1;
    private final static int MODO_EDITA_REGISTRO = 2;

    private final static int ACTUAL = 0;
    private final static int PRIMERO = 1;
    private final static int ATRAS = 2;
    private final static int ADELANTE = 3;
    private final static int ULTIMO = 4;

    @FXML
    private Button btnNuevo;
    @FXML
    private Button btnBorrarOAceptar;
    @FXML
    private Button btnEditarOCancelar;
    @FXML
    private Button btnBuscar;
    @FXML
    private TextField tfBuscar;
    @FXML
    private TextField tfID;
    @FXML
    private TextField tfNombre;
    @FXML
    private TextField tfDireccion;
    @FXML
    private Label lblInfo;

    //   private Connection con;
    //   private ResultSet rs;
    private Cliente cli;
    private int modo;
    // private OperacionesBD op;
    private clientesDAO cDAO;
    private List<Cliente> clientes;
    private int contador;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tfID.setDisable(true);

        try {
            tfBuscar.setEditable(true);
            cDAO = new clientesDAO();
            clientes = cDAO.findAll();
            contador = 0;
            cli = clientes.get(contador);
            System.out.println(cli);
            mostrarRegistro();
        } catch (SQLException ex) {
            System.out.println("Error en la conexión" + ex.getMessage());
        } catch (Exception ex) {
            System.out.println("Error Exeption controllerCliente " + ex);
        }

    }

    // ******************************************************************************
    // ACCIONES ASOCIADAS A BOTONES
    // ******************************************************************************
    @FXML
    private void accionPrimero() {

        cli = clientes.get(0);
        contador = 0;

        mostrarRegistro();
    }

    @FXML
    private void accionAtras() {

        if (contador > 0) {
            contador--;
            cli = clientes.get(contador);

            mostrarRegistro();
        }

    }

    @FXML
    private void accionAdelante() {

        if (contador < clientes.size()-1) {
            contador++;
            cli = clientes.get(contador);

            mostrarRegistro();
        }

    }

    @FXML
    private void accionUltimo() {

        contador = clientes.size() - 1;
        cli = clientes.get(contador);

        mostrarRegistro();

    }

    @FXML
    private void accionNuevo() {
        modo = MODO_NUEVO_REGISTRO;

        cambiarModo();

    }

    @FXML
    private void accionBuscar() {
        Cliente c;
        try {

            c = cDAO.findByPK(Integer.parseInt(tfBuscar.getText()));
            
            contador = clientes.indexOf(c);

            mostrarRegistro();

        } catch (Exception ex) {
            mensajeExcepcion(ex, "Error buscando");
        }
    }

    @FXML
    private void accionEditarOCancelar() {
        if (modo == MODO_NAVEGACION) {          // accion editar
            modo = MODO_EDITA_REGISTRO;
            cambiarModo();
        } else {                                // accion cancelar
            if (modo == MODO_NUEVO_REGISTRO) {

            }

            mostrarRegistro();
            modo = MODO_NAVEGACION;
            cambiarModo();
        }

    }

    @FXML
    private void accionBorrarOAceptar() {
        if (modo == MODO_NAVEGACION) {      // accion borrar
            String mensaje = "¿Estás seguro de borrar el registro [" + tfID.getText() + "]?";
            Alert d = new Alert(Alert.AlertType.CONFIRMATION, mensaje, ButtonType.YES, ButtonType.NO);
            d.setTitle("Borrado de registro");
            d.showAndWait();
            if (d.getResult() == ButtonType.YES) {
                try {

                    cDAO.delete(cli.getId());
                    System.out.println(cli.getId());
                    clientes.remove(cli);
                    contador--;

                    mostrarRegistro();
                } catch (SQLException ex) {
                    mensajeExcepcion(ex, "Borrando registro...");
                } catch (Exception ex) {
                    System.err.println("Error borrando: " + ex);
                }
            }

        } else {                            // accion aceptar
            try {
                Cliente c = new Cliente(tfNombre.getText(), tfDireccion.getText());
                if (modo == MODO_NUEVO_REGISTRO) {
                    cDAO.insert(c);
                    clientes.removeAll(clientes);
                    clientes = cDAO.findAll();
                    contador = clientes.size() - 1;
                    mostrarRegistro();
                } else {
                    System.out.println("hola actualitzar");
                    clientes.get(contador).setNombre(c.getNombre());
                    clientes.get(contador).setDireccion(c.getDireccion());
                    cDAO.update(clientes.get(contador));
                    clientes.removeAll(clientes);
                    clientes = cDAO.findAll();

                    mostrarRegistro();

                }

                modo = MODO_NAVEGACION;
                cambiarModo();
            } catch (SQLException ex) {
                mensajeExcepcion(ex, "Actualizando registro...");
            } catch (Exception ex) {
                System.out.println("Eror introduciendo datos: " + ex);
            }

        }
    }

    // ******************************************************************************
    // METODOS RELACIONADOS CON BD 
    // ******************************************************************************
    private void leerRegistro(int pos) {
        switch (pos) {
            case PRIMERO:
                contador = 0;
                cli = clientes.get(contador);
                break;
            case ATRAS:
                contador--;
                cli = clientes.get(contador);
                break;
            case ADELANTE:
                contador++;
                cli = clientes.get(contador);
                break;
            case ULTIMO:
                contador = clientes.size();
                cli = clientes.get(contador);
                break;
            case ACTUAL:
            default:
        }

    }

    private int totalRegistros() throws SQLException {

        int tot = clientes.size();

        return tot;
    }

    private void mostrarRegistro() {
        lblInfo.setText("Registro " + (contador + 1) + " de " + clientes.size());
        cli = clientes.get(contador);
        tfID.setText(String.valueOf(cli.getId()));
        tfNombre.setText(cli.getNombre());
        tfDireccion.setText(cli.getDireccion());
    }

    // ******************************************************************************
    // OTROS MÉTODOS
    // ******************************************************************************
    private void cambiarModo() {
        switch (modo) {
            case MODO_NAVEGACION:
                btnNuevo.setDisable(false);
                btnBorrarOAceptar.setText("Borrar");
                btnEditarOCancelar.setText("Editar");
                tfNombre.setEditable(false);
                tfDireccion.setEditable(false);
                tfBuscar.setEditable(true);
                break;
            case MODO_NUEVO_REGISTRO:
                tfID.setText("<autonum>");
                tfNombre.setText("");
                tfDireccion.setText("");
            case MODO_EDITA_REGISTRO:
                btnNuevo.setDisable(true);
                btnBorrarOAceptar.setText("Aceptar");
                btnEditarOCancelar.setText("Cancelar");
                tfNombre.setEditable(true);
                tfDireccion.setEditable(true);
                tfNombre.requestFocus();

        }

    }

    private void mensajeExcepcion(Exception ex, String msg) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Error de excepción");
        alert.setHeaderText(msg);
        alert.setContentText(ex.getMessage());

        String exceptionText = "";
        StackTraceElement[] stackTrace = ex.getStackTrace();
        for (StackTraceElement ste : stackTrace) {
            exceptionText = exceptionText + ste.toString() + System.getProperty("line.separator");
        }

        Label label = new Label("La traza de la excepción ha sido: ");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

}
