/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.articulosDAO;
import dao.grupoDAO;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import modelo.Articulo;
import modelo.Grupo;

/**
 *
 * @author carlos
 */
public class ControllerArticulo implements Initializable {

    private final static int MODO_NAVEGACION = 0;
    private final static int MODO_NUEVO_REGISTRO = 1;
    private final static int MODO_EDITA_REGISTRO = 2;
    private final static int ACTUAL = 0;
    private final static int PRIMERO = 1;
    private final static int ATRAS = 2;
    private final static int ADELANTE = 3;
    private final static int ULTIMO = 4;

    @FXML
    private Button btnNuevo;
    @FXML
    private Button btnBorrarOAceptar;
    @FXML
    private Button btnEditarOCancelar;
    @FXML
    private TextField tfID;
    @FXML
    private TextField tfNombre;
    @FXML
    private TextField tfPrecio;
    @FXML
    private TextField tfCodigo;
    @FXML
    private TextField tfGrupo;
    @FXML
    private ComboBox<Grupo> cmbGrupo;
    @FXML
    private Label lblInfo;

    private Articulo art;
    private int modo;
    private articulosDAO aDAO;
    private grupoDAO gDAO;
    private List<Articulo> articulos;
    private int contador;
    private Grupo grupo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tfID.setDisable(true);

        try {
            tfGrupo.setEditable(true);
            aDAO = new articulosDAO();
            gDAO  = new grupoDAO();
            articulos = aDAO.findAll();
            contador = 0;
            cmbGrupo.getItems().addAll(gDAO.findAll());
            art = articulos.get(contador);
            mostrarRegistro();
        } catch (SQLException ex) {
            System.out.println("Error en la conexión" + ex.getMessage());
        } catch (Exception ex) {
            System.out.println("Error Exeption " + ex);
        }

    }

    @FXML
    private void accionPrimero() {

        art = articulos.get(0);
        contador = 0;

        mostrarRegistro();
    }

    @FXML
    private void accionAtras() {
        if (contador > 0) {
            contador--;
            art = articulos.get(contador);

            mostrarRegistro();

        }

    }

    @FXML
    private void accionAdelante() {
        if (contador < articulos.size() - 1) {
            contador++;
            art = articulos.get(contador);

            mostrarRegistro();
        }
    }

    @FXML
    private void acctionNGrupo() {

        try {
            aDAO.insertGrupo(tfGrupo.getText());
            tfGrupo.setText("");
        } catch (Exception ex) {
            Logger.getLogger(ControllerArticulo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void accionUltimo() {

        contador = articulos.size() - 1;
        art = articulos.get(contador);

        mostrarRegistro();

    }

    @FXML
    private void accionNuevo() {
        modo = MODO_NUEVO_REGISTRO;

        cambiarModo();

    }

    @FXML
    private void accionEditarOCancelar() {
        if (modo == MODO_NAVEGACION) {          // accion editar
            modo = MODO_EDITA_REGISTRO;
            cambiarModo();
        } else {                                // accion cancelar
            if (modo == MODO_NUEVO_REGISTRO) {

            }

            mostrarRegistro();
            modo = MODO_NAVEGACION;
            cambiarModo();
        }

    }

    @FXML
    private void accionBorrarOAceptar() {
        if (modo == MODO_NAVEGACION) {      // accion borrar
            String mensaje = "¿Estás seguro de borrar el registro [" + tfID.getText() + "]?";
            Alert d = new Alert(Alert.AlertType.CONFIRMATION, mensaje, ButtonType.YES, ButtonType.NO);
            d.setTitle("Borrado de registro");
            d.showAndWait();
            if (d.getResult() == ButtonType.YES) {
                try {

                    aDAO.delete(art.getId());
                    System.out.println(art.getId());
                    articulos.remove(art);
                    contador--;

                    mostrarRegistro();
                } catch (SQLException ex) {
                    mensajeExcepcion(ex, "Borrando registro...");
                } catch (Exception ex) {
                    System.err.println("Error borrando: " + ex);
                }
            }

        } else {                            // accion aceptar
            try {
                Articulo c = new Articulo(tfNombre.getText(), Float.parseFloat(tfPrecio.getText()), tfCodigo.getText(), cmbGrupo.getValue().getId());
                if (modo == MODO_NUEVO_REGISTRO) {
                    aDAO.insert(c);
                    articulos.removeAll(articulos);
                    articulos = aDAO.findAll();
                    contador = articulos.size() - 1;
                    mostrarRegistro();
                } else {
                    System.out.println("hola actualitzar");
                    articulos.get(contador).setNombre(c.getNombre());
                    articulos.get(contador).setPrecio(c.getPrecio());
                    articulos.get(contador).setCodigo(c.getCodigo());
                    articulos.get(contador).setGrupo(c.getGrupo());
                    aDAO.update(articulos.get(contador));
                    System.out.println(c.getNombre() + c.getId());
                    articulos.removeAll(articulos);
                    articulos = aDAO.findAll();
                    // op.guardar(c, true);
                    mostrarRegistro();

                }

                modo = MODO_NAVEGACION;
                cambiarModo();
            } catch (SQLException ex) {
                mensajeExcepcion(ex, "Actualizando registro...");
            } catch (Exception ex) {
                System.out.println("Eror introduciendo datos: " + ex);
            }

        }
    }

    // ******************************************************************************
    // METODOS RELACIONADOS CON BD 
    // ******************************************************************************
    private void leerRegistro(int pos) {
        switch (pos) {
            case PRIMERO:
                contador = 0;
                art = articulos.get(contador);
                break;
            case ATRAS:
                contador--;
                art = articulos.get(contador);
                break;
            case ADELANTE:
                contador++;
                art = articulos.get(contador);
                break;
            case ULTIMO:
                contador = articulos.size();
                art = articulos.get(contador);
                break;
            case ACTUAL:
            default:
        }

    }

    private int totalRegistros() throws SQLException {

        int tot = articulos.size();

        return tot;
    }

    private void mostrarRegistro() {
        lblInfo.setText("Registro " + (contador + 1) + " de " + articulos.size());
        art = articulos.get(contador);
        Grupo grupo = new Grupo();

        try {
            grupo = gDAO.findByPK(art.getGrupo());
        } catch (Exception ex) {
            mensajeExcepcion(ex, "Error asignando el nombre del grupo");
        }
        tfID.setText(String.valueOf(art.getId()));
        tfNombre.setText(art.getNombre());
        tfPrecio.setText(String.valueOf(art.getPrecio()));
        tfCodigo.setText(art.getCodigo());
        cmbGrupo.setValue(grupo);
        
      

    }

    // ******************************************************************************
    // OTROS MÉTODOS
    // ******************************************************************************
    private void cambiarModo() {
        switch (modo) {
            case MODO_NAVEGACION:
                btnNuevo.setDisable(false);
                btnBorrarOAceptar.setText("Borrar");
                btnEditarOCancelar.setText("Editar");
                tfNombre.setEditable(false);
                tfPrecio.setEditable(false);
                tfCodigo.setEditable(false);
                break;
            case MODO_NUEVO_REGISTRO:
                tfID.setText("<autonum>");
                tfNombre.setText("");
                tfPrecio.setText("");
                tfCodigo.setText("");
            case MODO_EDITA_REGISTRO:
                btnNuevo.setDisable(true);
                btnBorrarOAceptar.setText("Aceptar");
                btnEditarOCancelar.setText("Cancelar");
                tfNombre.setEditable(true);
                tfPrecio.setEditable(true);
                tfCodigo.setEditable(true);
                tfNombre.requestFocus();

        }

    }

    private void mensajeExcepcion(Exception ex, String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error de excepción");
        alert.setHeaderText(msg);
        alert.setContentText(ex.getMessage());

        String exceptionText = "";
        StackTraceElement[] stackTrace = ex.getStackTrace();
        for (StackTraceElement ste : stackTrace) {
            exceptionText = exceptionText + ste.toString() + System.getProperty("line.separator");
        }

        Label label = new Label("La traza de la excepción ha sido: ");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

}
