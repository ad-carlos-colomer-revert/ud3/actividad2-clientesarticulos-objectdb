/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladorBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

/**
 *
 * @author carlos
 */
public class ConexionBD {

    private static EntityManagerFactory emf;
    private static EntityManager em;

    private static Connection con = null;

    public static EntityManager getConnection() throws SQLException {
        if (em == null) {

            try {
                emf = Persistence.createEntityManagerFactory("clientesArticulos.odb");
                em = emf.createEntityManager();
            } catch (PersistenceException e) {
                System.out.println("Problemón " + e.getMessage());
                System.exit(0);
            }

        }

        return em;

    }

    public static void cerrar() throws SQLException {

        if (em != null) {
            em.close();   // hace commit y cierra contenedor.
            emf.close();
        }

    }

}
