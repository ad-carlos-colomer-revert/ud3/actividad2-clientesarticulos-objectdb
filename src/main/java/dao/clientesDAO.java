/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import controladorBD.ConexionBD;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import modelo.Cliente;

/**
 *
 * @author carlos
 */
public class clientesDAO implements GenericoDAO {

   
    private EntityManager em;

    public clientesDAO() {

        try {
            em = ConexionBD.getConnection();
//            Cliente c = new Cliente ("carlos", "fase");
//            em.getTransaction().begin();
//        em.persist(c);
//        em.getTransaction().commit();
            
        } catch (SQLException ex) {
            System.out.println("Error Conexión " + ex);
        }

    }

    @Override
    public Cliente findByPK(int id) throws Exception {

        Cliente c = null;

        TypedQuery<Cliente> query = em.createQuery("SELECT p FROM Cliente p where id = " + id, Cliente.class);
        List<Cliente> cliente = query.getResultList();
        
        for (Cliente p : cliente) {
           c = new Cliente(p.getId(),p.getNombre(), p.getDireccion());
             
        }
        return c;
    }

    /**
     *
     * @return @throws Exception
     */
    @Override
    public List<Cliente> findAll() throws Exception {
        System.out.println("Hola find");
       TypedQuery<Cliente> query = em.createQuery("SELECT p FROM Cliente p", Cliente.class);
        List<Cliente> results = query.getResultList();
        for (Cliente p : results) {
            System.out.println(results.size());
           
            System.out.println(p);
        }
        return results;

    }

    @Override
    public List<Cliente> findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(Object t) throws Exception {
        Cliente c = (Cliente) t;

        em.getTransaction().begin();
        em.persist(c);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {

        Cliente c = (Cliente) t;
        boolean update;

        em.getTransaction().begin();
        /*
        El codi de baix comentat es perque sense ell, en el que es queda descomentat, funciona l'update de manera correcta.
        Al funcionar aixì, no sabía molt be com fer-ho de la manera que has explicat tu en classe d'agafar l'objecte, el id, i actualizarlo.
        Ho he intentat, i recupere el client de la base de datos, pero no tinc molt clar com actualitzar amb el set, per que
        per a això tindría que tindre dos clients no? Un que is de la base de datos, i altre que es el que li pase jo,
        el que jo he fet ha sigut crear un client nou, i mantenir el id que rep desde la base de datos, posarli els valor que rep del
        client c que son els datos que posa l'usuari, pero al funcionar directament sense tot això, no se si es correcte o no
        */
        
//        System.out.println("Actualizar");
//        TypedQuery<Cliente> query = em.createQuery("SELECT p FROM Cliente p where id = " + c.getId(), Cliente.class);
//        List<Cliente> results = query.getResultList();
//        Cliente cli;
//        for (Cliente p : results) {
//           cli = new Cliente(p.getId(),c.getNombre(), c.getDireccion());
//           
//           
//            
//        }
        em.getTransaction().commit();

        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {

        TypedQuery<Cliente> query = em.createQuery("SELECT p FROM Cliente p", Cliente.class);
        List<Cliente> results = query.getResultList();
        em.getTransaction().begin();

        for (Cliente cliRemove : results) {
            em.remove(cliRemove);
            System.out.println("Borrado " + cliRemove);
        }
        em.getTransaction().commit();

        return true;
    }

}
