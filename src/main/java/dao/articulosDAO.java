/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import controladorBD.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import modelo.Articulo;
import modelo.Cliente;
import modelo.Grupo;

/**
 *
 * @author carlos
 */
public class articulosDAO implements GenericoDAO {

    private EntityManager em;

    public articulosDAO() {
        try {
            em = ConexionBD.getConnection();
//            Grupo g = new Grupo("Software");
//            em.getTransaction().begin();
//            em.persist(g);
//            em.getTransaction().commit();
//            Articulo a = new Articulo("Windows 25", 150.56F, "ERS344", 30);
//            em.getTransaction().begin();
//            em.persist(a);
//            em.getTransaction().commit();
        } catch (SQLException ex) {
            System.out.println("Error Conexión " + ex);
        }
    }

    @Override
    public Object findByPK(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Articulo> findAll() throws Exception {
        TypedQuery<Articulo> query = em.createQuery("SELECT p FROM Articulo p", Articulo.class);
        List<Articulo> results = query.getResultList();
        for (Articulo p : results) {
            System.out.println(results.size());

            System.out.println(p);
        }
        return results;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(Object t) throws Exception {
        Articulo a = (Articulo) t;

        em.getTransaction().begin();
        em.persist(a);
        em.getTransaction().commit();
        return true;
    }

    public boolean insertGrupo(String t) throws Exception {
        Grupo g = new Grupo(t);

        em.getTransaction().begin();
        em.persist(g);
        em.getTransaction().commit();

        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        Articulo a = (Articulo) t;
        boolean update;
        //Pasa el mateix que a l'update de clients
        em.getTransaction().begin();

        em.getTransaction().commit();

        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        TypedQuery<Articulo> query = em.createQuery("SELECT p FROM Articulo p", Articulo.class);
        List<Articulo> results = query.getResultList();
        em.getTransaction().begin();

        for (Articulo artRemove : results) {
            em.remove(artRemove);
            System.out.println("Borrado " + artRemove);
        }
        em.getTransaction().commit();

        return true;
    }

}
