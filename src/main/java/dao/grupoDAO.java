/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import controladorBD.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import modelo.Grupo;

/**
 *
 * @author carlos
 */
public class grupoDAO implements GenericoDAO {

    private EntityManager em;

    public grupoDAO() {
        try {
            em = ConexionBD.getConnection();
        } catch (SQLException ex) {
            System.out.println("Error Conexión " + ex);
        }
    }

    @Override
    public Grupo findByPK(int id) throws Exception {
        Grupo g = null;

        TypedQuery<Grupo> query = em.createQuery("SELECT p FROM Grupo p where id = " + id, Grupo.class);
        List<Grupo> cliente = query.getResultList();

        for (Grupo p : cliente) {
            g = new Grupo(p.getId(), p.getDescripcion());

        }

        return g;
    }

    @Override
    public List<Grupo> findAll() throws Exception {
        TypedQuery<Grupo> query = em.createQuery("SELECT p FROM Grupo p", Grupo.class);
        List<Grupo> results = query.getResultList();
        for (Grupo p : results) {
            System.out.println(results.size());

            System.out.println(p);
        }
        return results;
    }

    @Override
    public List findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(Object t) throws Exception {
        Grupo g = (Grupo) t;
        em.getTransaction().begin();
        em.persist(g);
        em.getTransaction().commit();

        return true;
    }

    @Override
    public boolean update(Object t) throws Exception {
        Grupo g = (Grupo) t;
        
         //Pasa el mateix que a l'update de clients
        em.getTransaction().begin();
        em.getTransaction().commit();

        return true;

    }

    @Override
    public boolean delete(int id) throws Exception {

        TypedQuery<Grupo> query = em.createQuery("SELECT p FROM Grupo p", Grupo.class);
        List<Grupo> results = query.getResultList();
        em.getTransaction().begin();

        for (Grupo grupoRemove : results) {
            em.remove(grupoRemove);
            System.out.println("Borrado " + grupoRemove);
        }
        em.getTransaction().commit();

        return true;
    }

}
